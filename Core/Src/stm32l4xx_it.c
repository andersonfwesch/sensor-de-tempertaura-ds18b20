//***************** (C) COPYRIGHT 2021 Ânderson F.W *****************
/// @brief		Funções de atendimento as interrupções
/// @file 		stm32l4xx_hal_it.c
/// @author		Ânderson F. Weschenfelder
/// @version	V1.00
/// @date		02/08/21
// *****************************************************************************
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_it.h"

// Private typedef -------------------------------------------------------------
// Private define --------------------------------------------------------------
// Private macro ---------------------------------------------------------------

// Private variables -----------------------------------------------------------
extern TIM_HandleTypeDef htim6;
extern UART_HandleTypeDef huart2;

// Private function prototypes -------------------------------------------------

// Private functions -----------------------------------------------------------

// *****************************************************************************
/// @brief	Funçâo de atendimento a interrupções não mascáraveis
// *****************************************************************************
void NMI_Handler(void)
{
  while (1)
  {
  }
}
// *****************************************************************************
/// @brief	Funçâo de atendimento de falha do hardware
// *****************************************************************************
void HardFault_Handler(void)
{
	NVIC_SystemReset();
}

// *****************************************************************************
/// @brief	Funçâo de atendimento da gestão da memória
// *****************************************************************************
void MemManage_Handler(void)
{
	NVIC_SystemReset();
}

// *****************************************************************************
/// @brief	Funçâo de atendimento de falha do barramento
// *****************************************************************************
void BusFault_Handler(void)
{
	NVIC_SystemReset();
}
// *****************************************************************************
/// @brief	Funçâo de atendimento de falha de uso
// *****************************************************************************
void UsageFault_Handler(void)
{
	NVIC_SystemReset();
}
// *****************************************************************************
/// @brief	Função de atendimento a chamadas do serviço de sistema via SWI Instrução
// *****************************************************************************
void SVC_Handler(void)
{

}
// *****************************************************************************
/// @brief	Função de atendimento do monitor de debug
// *****************************************************************************
void DebugMon_Handler(void)
{
	NVIC_SystemReset();
}
// *****************************************************************************
/// @brief	Função de atendimento a pendências do serviço de sistema
// *****************************************************************************
void PendSV_Handler(void)
{

}
// *****************************************************************************
/// @brief	Funçãoo de atendimento da interrupção do relógio do sistema
// *****************************************************************************
void SysTick_Handler(void)
{
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
}
// *****************************************************************************
/// @brief	Funçãoo de atendimento da interrupção da UART (terminal)
// *****************************************************************************
void USART2_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart2);
}
// *****************************************************************************
/// @brief	Funçãoo de atendimento da interrupção externa (button)
// *****************************************************************************
void EXTI15_10_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}
// *****************************************************************************
/// @brief	Funçãoo de atendimento da interrupção do conversor AD
// *****************************************************************************
void TIM6_DAC_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim6);
}
//***************** (C) COPYRIGHT 2021 Ânderson F.W *****END OF FILE*********
