//***************** (C) COPYRIGHT 2021 Ânderson F.W *****************
/// @brief
/// @file 		main.h
/// @author		Ânderson F. Weschenfelder
/// @version	V1.00
/// @date		02/08/21
// *****************************************************************************
// Prevençâo contra inclusão recursiva -----------------------------------------
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

// Includes --------------------------------------------------------------------
#include "stm32l4xx_hal.h"

// Exported constants ----------------------------------------------------------
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define ds18b20_Pin GPIO_PIN_10
#define ds18b20_GPIO_Port GPIOC
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB

// Private variables -----------------------------------------------------------
extern TIM_HandleTypeDef htim6;

// Exported macro --------------------------------------------------------------

// Exported functions ----------------------------------------------------------
void Error_Handler(void);
void SystemClock_Config(void);
void MX_GPIO_Init(void);
void MX_USART2_UART_Init(void);
void MX_CRC_Init(void);
void MX_TIM6_Init(void);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

//***************** (C) COPYRIGHT 2021 Ânderson F.W *****END OF FILE*********
