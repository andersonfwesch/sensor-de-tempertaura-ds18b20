//***************** (C) COPYRIGHT 2021 Ânderson F.W *****************
/// @brief
/// @file 		stm32l4xx_it.h
/// @author		Ânderson F. Weschenfelder
/// @version	V1.00
/// @date		02/08/21
// *****************************************************************************

// Prevenção contra inclusão recursiva -----------------------------------------
#ifndef __STM32L4xx_IT_H
#define __STM32L4xx_IT_H

#ifdef __cplusplus
 extern "C" {
#endif

 // Includes --------------------------------------------------------------------

 // Exported constants ----------------------------------------------------------

 // Exported functions ----------------------------------------------------------
void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void SysTick_Handler(void);
void USART2_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void TIM6_DAC_IRQHandler(void);


#ifdef __cplusplus
}
#endif

#endif /* __STM32L4xx_IT_H */

 //***************** (C) COPYRIGHT 2021 Ânderson F.W *****END OF FILE*********
